/**
 * Created by Mita on 3.2.2018..
 */
jQuery(function ($) {
    const socket = io();

    $('#btnStart').click(() => {
        if ($('#lengthInput').val().length > 0) {
            socket.emit('startRelay', Number($('#lengthInput').val()));
        }
    });

    socket.on('currentPosition', function (msg) {
        $('#currentLength').val(Number(msg));
    });
});