/**
 * Created by Mita on 3.2.2018..
 */
var io = require('socket.io');
var rotaryEncoder = require('onoff-rotary');
var Gpio = require('onoff').Gpio;
const myEncoder = rotaryEncoder(5, 6);

const relayUP = new Gpio(17, 'out');
const relayDOWN = new Gpio(18, 'out');
let pulsePosition = 0;
let globalNextPosition;
let isStartRelay = false;

myEncoder.on('rotation', function (direction) {
    if (direction > 0) {
        // console.log('Encoder rotated right');
        pulsePosition++;
    } else {
        // console.log('Encoder rotated left');
        pulsePosition--;
    }
    io.emit('currentPosition', pulsePosition);
    if (isStartRelay) {
        if (pulsePosition === globalNextPosition) {
            relayDOWN.writeSync(0);
            relayUP.writeSync(0);
        }
    }
});

function startRelay(nextPosition) {
    globalNextPosition = nextPosition;
    if (pulsePosition < nextPosition) {
        relayDOWN.writeSync(0);
        relayUP.writeSync(1);

    } else {
        relayUP.writeSync(0);
        relayDOWN.writeSync(1);
    }
}


exports.setIoServer = function (server) {
    io = io.listen(server);

    io.on('connection', function (client) {
        console.log("Client id:" + client.id + " client address: " + client.handshake.address + " connected!");

        client.on('startRelay', function (message) {
            console.log(message);
            isStartRelay = true;
            startRelay(Number(message));
        });


        client.on('disconnect', function () {
            console.log("Client id:" + client.id + " client address: " + client.handshake.address + " disconnected!");
        });
    });
};
